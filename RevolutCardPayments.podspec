Pod::Spec.new do |spec|
    spec.name = 'RevolutCardPayments'
    spec.version = '1.1.0'
    spec.summary = 'Revolut - RevolutCardPayments'
    spec.homepage = 'https://bitbucket.org/revolut/revolut-card-payments-ios'
    spec.source = { :git => 'https://bitbucket.org/revolut/revolut-card-payments-ios', :tag => spec.version.to_s }
    spec.license = { :type => 'Custom', :file => 'LICENSE' }
    spec.author = { 'Revolut' => 'team@revolut.com' }
    spec.swift_version = '5.0'
    spec.static_framework = true
    spec.ios.deployment_target = '13.0'

    spec.resources = [
        'RevolutCardPayments/RevolutCardPayments.bundle',
        'RevolutCardPayments/Money.bundle',
    ]

    spec.vendored_frameworks = [
        'RevolutCardPayments/RevolutCardPayments.xcframework',
    ]
end