# DEPRECATED
This version of the SDK is no longer maintained.
From now on, in order to implement the Revolut Merchant Card Form SDK, please use [Revolut Payments](https://bitbucket.org/revolut/revolut-payments-ios/src/master/).
## Migration Guide
Migrating to the new version is easy and fast. You must only follow these two steps:
1.  In your `Podfile`, change the following line from:
```ruby
pod 'RevolutCardPayments'
```
to:
```ruby
pod 'RevolutPayments/RevolutMerchantCardForm'
```
2. In your Xcode project, modify the imports of `RevolutPay` to `RevolutPayments`.
---

# **Revolut Card Payments SDK - iOS documentation**

The Revolut Payments SDK for iOS lets you accept card payments directly from your app. It is designed for easy implementation and usage. The SDK simplifies the process to initiate a card payment and interact with the Revolut backend to verify the payment status.

##### NOTE

In order to use and accept card payment with Revolut, you need to have been [accepted as a Merchant](https://www.revolut.com/business/help/merchant-accounts/getting-started/how-do-i-apply-for-a-merchant-account) in your [Revolut Business](https://business.revolut.com/merchant) account.

# Get started with the Revolut Card Payments SDK for iOS

Set up the Revolut Card Payments SDK to accept card payments directly in your app. It requires 4 steps:

1. Install the SDK
2. Get your merchant API key
3. Create an order
4. Provide card details to the SDK and start payment.

## 1. Install the SDK

#### A) Installation:
#####  Manual Instalation

Drag and drop RevolutCardPayments folder into your project.

#####  Cocoapods Instalation

To integrate Revolut Card Payments SDK into your Xcode project using CocoaPods, add to your `Podfile`:

```
pod 'RevolutCardPayments'
```

#### B) Import RevolutCardPayments module with:
 
```
import RevolutCardPayments
```

#### C) Create a RevolutCardPayments object with:

```
let revolutCardPaymentsKit = RevolutCardPaymentsKit()
```

##### NOTE

You can provide environment to the initialisation method. By default it's `production`, but the SDK also supports `sandbox` for testing purposes. 

## 2. Get your merchant API key

Go to your Revolut app to generate the [Merchant API key](https://business.revolut.com/settings/merchant-api). You need it as part of the authorisation header for each Merchant API request.

##### NOTE

Use this key ****only**** for the production environment. For the [Revolut Business Sandbox environment](https://sandbox-business.revolut.com/), use the [sandbox API key](https://sandbox-business.revolut.com/settings/merchant-api). For more information, see [Test in the Sandbox environment](https://developer.revolut.com/docs/accept-payments/tutorials/test-in-the-sandbox-environment/configuration)

## 3. Create an order

When a user decides to make a purchase on your app, on the server side you need to create an order by sending a `POST` request to `https://merchant.revolut.com/api/1.0/orders`. You must include the authorization header in the request, which is in the following format:

`Bearer [yourAPIKey]`

Where `[yourAPIKey]` is the production API key that you [generated from your Merchant account](https://business.revolut.com/settings/merchant-api).

Server side: Create an order via the Merchant API request:

```
curl -X "POST" "https://merchant.revolut.com/api/1.0/orders" \
   -H 'Authorization: Bearer [yourApiKey]' \
   -H 'Content-Type: application/json; charset=utf-8' \
   -d $'{
     "amount": 100,
     "currency": "GBP",
     "customer": {
        "id": [customerId]
     }
    }'
```

When the order is created successfully, the Merchant API returns a JSON array in the [response](https://developer.revolut.com/api-reference/merchant/#operation/createOrder) that looks like this:

```
{
 "id": "<ID>",
 "public_id": "<PUBLIC_ID>",
 "type": "PAYMENT",
 "state": "PENDING",
 "created_date": "2020-10-15T07:46:40.648108Z",
 "updated_date": "2020-10-15T07:46:40.648108Z",
 "order_amount": {
  "value": 100,
  "currency": "GBP"
 }
 "customer": {
        "id": "<CUSTOMER_ID>",
        ...
    }
}
```

You should save the `public_id` which will be used to start a payment later on.

##### NOTE
A new order must be created for each purchase.  

For more details check our [public documentation](https://developer.revolut.com/docs/accept-payments).

## 4. Start the payment
To capture the payment from your customer, you need to:

**a)** Run `pay` method providing the order `public_id`, which will open a form where your customer can provide all required card details. Once form is filled up and customer taps on `Pay` button the payment is initiated.

**b)** Eventually you will receive the result of the payment in the completion callback, which will provide the status (success or failure)


Payment example:

```
    revolutCardPaymentsKit.pay(
        publicId: <PUBLIC_ID>,
        billingAddress: .init(
            country: "GB",
            region: "Greater London",
            city: "London",
            streetLine1: "1 Canada Square",
            streetLine2: "Revolut",
            postcode: "11111"
        ),
        shippingAddress: ... // optional shipping address,
        email: ... // optional email,
        savePaymentMethodFor: ... // optional, can be set to `customer` or `merchant`,
        presentFrom: navigationController,
        completion: { result in
            
        }
    )
```

**email** - If not provided here then the form will containt an `email` input field where customer can provide the email

**completion** -  It's invoked when the payment is finally proceeded. It completes with the result of the payment, which can be either success or failure. In case of the failure it provides a `PaymentError`

### PaymentError

Possible values of `PaymentError`:

- failed(FailureReasonError)
- declined(FailureReasonError)
- internalError
- timeout
- invalidInput(Set<InputValidationError>)

where  for  `FailureReasonError` possible values are:

- highRisk
- unknownCard
- pickupCard
- invalidCard
- expiredCard
- doNotHonour
- invalidEmail
- invalidAmount
- restrictedCard
- insufficientFunds
- rejectedByCustomer
- cardholderNameMissing
- withdrawalLimitExceeded
- threeDSChallengeFailed
- threeDSChallengeAbandoned
- threeDSChallengeFailedManually
- transactionNotAllowedForCardholder
- unknown

and for `InputValidationError`:

- invalidCardholderName
- invalidExpirationDate
- invalidCardNumber
- invalidCVV
- invalidEmail
