//
//  ViewController.swift
//  RevolutCardPaymentsApp
//
//  Created by Revolut on 09/02/2022.
//

import UIKit
import RevolutCardPayments

final class SampleViewController: UIViewController {
    /// `environment` can be either set to `sandbox` or `production` (default value)
    private let revolutCardPaymentsKit = RevolutCardPaymentsKit(environment: .sandbox)

    private lazy var publicIdTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "order public id"
        textField.borderStyle = .roundedRect
        textField.backgroundColor = .secondarySystemBackground
        return textField
    }()

    private lazy var payWithCardButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Pay with card", for: .normal)
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }

    @objc private func buttonTapped() {
        guard let publicId =
            publicIdTextField
            .text?
            .trimmingCharacters(in: .whitespacesAndNewlines) else { return }

        revolutCardPaymentsKit.pay(
            publicId: publicId,
            billingAddress: .init(
                country: "GB",
                region: "Greater London",
                city: "London",
                streetLine1: "1 Canada Square",
                streetLine2: nil,
                postcode: "SW1A 1AA"
            ),
            shippingAddress: nil,
            email: nil,
            savePaymentMethodFor: .customer,
            presentFrom: self,
            completion: { [weak self] result in
                self?.showOutcome(result)
            }
        )
    }
}

// MARK: - Helpers
private extension SampleViewController {
    func setupViews() {
        view.backgroundColor = .systemBackground
        view.addSubview(publicIdTextField)
        view.addSubview(payWithCardButton)

        NSLayoutConstraint.activate([
            publicIdTextField.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16),
            publicIdTextField.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -16),
            publicIdTextField.heightAnchor.constraint(equalToConstant: 56),
            payWithCardButton.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            payWithCardButton.topAnchor.constraint(equalTo: publicIdTextField.bottomAnchor, constant: 32),
            payWithCardButton.leadingAnchor.constraint(equalTo: publicIdTextField.leadingAnchor),
            payWithCardButton.trailingAnchor.constraint(equalTo: publicIdTextField.trailingAnchor)
        ])
    }

    func showOutcome(_ outcome: Result<Void, RevolutCardPaymentsKit.PaymentError>) {
        let message: String
        switch outcome {
        case .success:
            message = "Success"
        case .failure(.internalError):
            message = "Internal Error"
        case .failure(.orderNotFound):
            message = "Order Not Found"
        case .failure(.orderNotAvailable):
            message = "Order Not Available"
        case let .failure(.declined(reason)):
            message = "Declined: \(reason)"
        case let .failure(.failed(reason)):
            message = "Failed: \(reason)"
        case .failure(.timeout):
            message = "Timeout"
        case let .failure(.invalidInput(errors)):
            let errorDescriptions = errors
                .map {
                    "\($0)"
                }
                .joined(separator: "\n")
            message = "Invalid input:\n" + errorDescriptions
        case .failure:
            message = "Error"
        }

        let alert = UIAlertController(
            title: "Outcome",
            message: message,
            preferredStyle: .alert
        )

        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))

        self.present(
            alert,
            animated: true
        )
    }
}
